﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;


namespace npSpot {
    [Guid("D2D2B33E-A731-42C1-AC96-E59473268D51")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface InpSpot {
        [DispId(1)]
        string np();
    }

    [Guid("FD8941CA-C5C1-4D88-B8BB-55BFDE95709A")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("npspot.np")]
    public class npSpot : InpSpot {
        public string np() {
            foreach (var process in System.Diagnostics.Process.GetProcessesByName("Spotify")) {
                var title = process.MainWindowTitle;

                Regex npRegex = new Regex(@".*Spotify - (.*?) – (.*)");

                if (npRegex.IsMatch(title.ToString())) {
                    Match Match = npRegex.Match(title);
                    string artist = Match.Groups[1].Value;
                    string song = Match.Groups[2].Value;

                    string songString = "[kSpot] np: " + song + " by " + artist;

                    return songString;
                } else {
                    return title;
                }
            }
            return "Not Playing";
        }
        public void unload() {
            AppDomain.Unload(AppDomain.CurrentDomain);
        }
    }


}
