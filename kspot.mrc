on *:START: {
  npSpotInit
}

alias npSpotInit {
  comopen npSpot npSpot.np
}

alias npSpotDie {
  $com(npSpot, unload, 1)
  comclose npSpot
}

alias np {
  $com(npSpot, np, 1)
  me $comcall(npSpot).result
}
